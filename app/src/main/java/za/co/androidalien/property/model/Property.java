package za.co.androidalien.property.model;

import android.os.Parcel;
import android.os.Parcelable;


public class Property implements Parcelable {

    public String title;
    public String city;
    public String suburb;
    public int bedrooms;

    public Property() {
    }

    public Property(String title, String city, String suburb, int bedrooms) {
        this.title = title;
        this.city = city;
        this.suburb = suburb;
        this.bedrooms = bedrooms;
    }

    protected Property(Parcel in) {
        title = in.readString();
        city = in.readString();
        suburb = in.readString();
        bedrooms = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(city);
        dest.writeString(suburb);
        dest.writeInt(bedrooms);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Property property = (Property) o;

        if (bedrooms != property.bedrooms) return false;
        if (title != null ? !title.equals(property.title) : property.title != null) return false;
        if (city != null ? !city.equals(property.city) : property.city != null) return false;
        if (suburb != null ? !suburb.equals(property.suburb) : property.suburb != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (suburb != null ? suburb.hashCode() : 0);
        result = 31 * result + bedrooms;
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Property> CREATOR = new Creator<Property>() {
        @Override
        public Property createFromParcel(Parcel in) {
            return new Property(in);
        }

        @Override
        public Property[] newArray(int size) {
            return new Property[size];
        }
    };
}
