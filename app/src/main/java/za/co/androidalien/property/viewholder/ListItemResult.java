package za.co.androidalien.property.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import za.co.androidalien.property.R;


public class ListItemResult extends BaseViewHolder {

    @BindView(R.id.list_item_title)
    TextView title;
    @BindView(R.id.list_item_city)
    TextView city;
    @BindView(R.id.list_item_subrub)
    TextView suburb;
    @BindView(R.id.list_item_bedrooms)
    TextView bedrooms;

    public ListItemResult(View itemView) {
        super(itemView);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setCity(String city) {
        this.city.setText(city);
    }

    public void setSuburb(String suburb) {
        this.suburb.setText(suburb);
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms.setText("Bedrooms: " + bedrooms);
    }
}
