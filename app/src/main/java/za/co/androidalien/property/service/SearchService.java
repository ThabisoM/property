package za.co.androidalien.property.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;

import za.co.androidalien.property.model.Property;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SearchService extends IntentService {

    private static final String ACTION_SEARCH = "za.co.androidalien.property.activity.service.action.SEARCH";

    private static final String EXTRA_PARAM = "za.co.androidalien.property.activity.service.extra.PARAM";
    public static final String ACTION_RESULT = "ACTION_RESULT";
    public static final String EXTRA_RESULT = "EXTRA_RESULT";


    public SearchService() {
        super("SearchService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionSearch(Context context, String param1) {
        Intent intent = new Intent(context, SearchService.class);
        intent.setAction(ACTION_SEARCH);
        intent.putExtra(EXTRA_PARAM, param1);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SEARCH.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM);
                handleActionSearch(param1);
            }
        }
    }

    private void handleActionSearch(String param1) {

        ArrayList<Property> list = initialiseList();

        Intent resultIntent = new Intent();
        resultIntent.setAction(ACTION_RESULT);
        resultIntent.putExtra(EXTRA_RESULT, list);
        LocalBroadcastManager.getInstance(this).sendBroadcast(resultIntent);

    }

    private ArrayList<Property> initialiseList() {

        ArrayList<Property> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Property property = new Property();

            property.title = "Property " + i;
            property.city = "City " + i;
            property.suburb = "Suburb " + i;
            property.bedrooms = i;

            list.add(property);
        }

        return list;

    }

}
