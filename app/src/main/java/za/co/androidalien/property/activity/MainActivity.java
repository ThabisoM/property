package za.co.androidalien.property.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import za.co.androidalien.property.R;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btnsearch)
    Button btnSearch;

    @BindView(R.id.et_search)
    EditText etSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (btnSearch != null) {
            btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText = etSearch.getText().toString().trim();

                Intent searchIntent = new Intent(MainActivity.this, ResultActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("SEARCH_STRING", searchText);

                searchIntent.putExtra("SEARCH_BUNDLE", bundle);

                startActivity(searchIntent);

            }
        });
        }

    }

//    private void setOnclicks() {
//
//    }


    @Override
    public void onClick(View view) {

    }
}
