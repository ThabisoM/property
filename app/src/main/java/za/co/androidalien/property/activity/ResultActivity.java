package za.co.androidalien.property.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import za.co.androidalien.property.R;
import za.co.androidalien.property.adapter.ResultAdapter;
import za.co.androidalien.property.model.Property;
import za.co.androidalien.property.service.SearchService;

public class ResultActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    ResultAdapter mAdapter;
    ArrayList<Property> resultList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mAdapter = new ResultAdapter();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        SearchService.startActionSearch(this, "test");
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(SearchService.ACTION_RESULT);

        LocalBroadcastManager.getInstance(this).registerReceiver(resultReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(resultReceiver);
    }

    private BroadcastReceiver resultReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {

                case SearchService.ACTION_RESULT:
                    resultList = intent.getParcelableArrayListExtra(SearchService.EXTRA_RESULT);

                    if (resultList.size() > 0) {
                        mAdapter.setData(resultList);
                    } else {
                        //Display no results message
                    }
                    break;
            }

        }
    };
}
