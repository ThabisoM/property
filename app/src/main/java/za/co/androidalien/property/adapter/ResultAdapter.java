package za.co.androidalien.property.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import za.co.androidalien.property.R;
import za.co.androidalien.property.model.Property;
import za.co.androidalien.property.viewholder.ListItemResult;


public class ResultAdapter extends RecyclerView.Adapter<ListItemResult> {

    private ArrayList<Property> list;

    public ResultAdapter() {

        list = new ArrayList<>();
    }

    public void setData(ArrayList<Property> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ListItemResult onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ListItemResult(inflater.inflate(R.layout.list_item_result, parent, false));
    }

    @Override
    public void onBindViewHolder(ListItemResult holder, int position) {

        Property property = list.get(position);

        holder.setTitle(property.title);
        holder.setCity(property.city);
        holder.setSuburb(property.suburb);
        holder.setBedrooms(property.bedrooms);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
